from __future__ import absolute_import
from __future__ import unicode_literals
from markdown import Extension
from markdown.util import etree, AtomicString
from markdown.inlinepatterns import BacktickPattern, BACKTICK_RE
from markdown.blockprocessors import BlockProcessor

import json

class GraphProcessor(BlockProcessor):
    def test(self, parent, block):
        is_graph = block.startswith('graph:')
        # Pelican expects an array with a single value here 
        self.parser.markdown.Meta['graphs'] = [self.parser.markdown.Meta.get('graphs', [False])[0] or is_graph]
        return is_graph

    def run(self, parent, blocks):
        block = blocks.pop(0)
        data = {'remotes': []}
        for line in block.strip().split('\n')[1:]:
            key, value = line.strip().split(':', 1)
            if key.startswith('remote-'):
                _, num, key = key.split('-')
                num = int(num)
                while len(data['remotes']) <= num:
                    data['remotes'].append({'remote': True})
                data['remotes'][num][key.strip()] = value.strip()
            else:
                data[key.strip()] = value.strip()
        self.fixdata(data)
        for remote in data.get('remotes', []):
            self.fixdata(remote)
            remote.update({
                'name': '%s_remote_%s' % (data['name'], remote['name']),
                'remoteName': remote['name'],
                'width': 300,
                'commitRadius': 15,
            })
        div = etree.SubElement(parent, 'div')
        div.set('class', 'graph')
        div.set('id', 'graph-%s' % data['name'])
        script = etree.SubElement(div, 'script')
        script.set('type', 'text/javascript')
        script.text = self.parser.markdown.htmlStash.store('''
            require(['graphs'], function(Graph) {
                window.addEventListener('load', function() {
                    var graph = %s;
                    all_graphs.%s = new Graph(graph, '#graph-%s').reset();
                }, false);
            });
        ''' % (json.dumps(data), data['name'], data['name']), safe=True)

    def fixdata(self, data):
        commits, data['commitData'] = data.pop('commits'), []
        refs_, refs = data.pop('refs'), {}
        for ref in refs_.split(';'):
            ref, tgt = ref.split('=>')
            refs[ref.strip()] = tgt.strip()
        for commit in commits.split(';'):
            if '=>' not in commit:
                commit, parents = commit.strip(), []
            else:
                commit, parents = commit.split('=>')
                commit = commit.strip()
                parents = [p.strip() for p in parents.split(',')]
            crefs = [ref for ref in refs if refs[ref] == commit]
            data['commitData'].append({'idx': commit, 'refs': crefs, 'parents': parents})
        data['items'] = [int(x) for x in data['items'].split(',')]

class GraphCommandPattern(BacktickPattern):
    def handleMatch(self, m):
        if '!' not in m.group(3):
            return super(GraphCommandPattern, self).handleMatch(m)
        cmd, js = m.group(3).strip().split('!', 1)
        graph, command = js.split('.', 1)
        js = 'all_graphs.%s.historyView.%s' % (graph, command)
        wrapper = etree.Element('span')
        el = etree.SubElement(wrapper, self.tag)
        el.set('class', 'interactive')
        el.set('onClick', js)
        el.text = AtomicString(cmd)
        icon = etree.SubElement(el, 'span')
        icon.set('class', 'fa fa-play-circle-o')
        return wrapper

class GraphsExtension(Extension):
    def extendMarkdown(self, md, md_globals):
        md.registerExtension(self)
        md.parser.blockprocessors.add("graphs", GraphProcessor(md.parser), "_begin")
        md.inlinePatterns['backtick'] = GraphCommandPattern(BACKTICK_RE)

def makeExtension(*args, **kwargs):
    return GraphsExtension(*args, **kwargs)
