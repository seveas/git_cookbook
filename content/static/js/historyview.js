define(['d3', 'seedrandom'], function (d3, seedrandom) {
    "use strict";

    var REG_MARKER_END = 'url(#triangle)',
        MERGE_MARKER_END = 'url(#brown-triangle)',
        FADED_MARKER_END = 'url(#faded-triangle)',

        preventOverlap,
        applyBranchlessClass,
        cx, cy, fixCirclePosition,
        px1, py1, fixPointerStartPosition,
        px2, py2, fixPointerEndPosition,
        fixIdPosition, refY;

    preventOverlap = function preventOverlap(commit, view) {
        var commitData = view.commitData,
            baseLine = view.baseLine,
            shift = view.commitRadius * 4.5,
            overlapped = null;

        for (var i = 0; i < commitData.length; i++) {
            var c = commitData[i];
            if (c.cx === commit.cx && c.cy === commit.cy && c !== commit) {
                overlapped = c;
                break;
            }
        }

        if (overlapped) {
            var oParent = overlapped.parents[0],
                parent = commit.parents[0];

            if (overlapped.cy < baseLine) {
                overlapped = oParent.cy < parent.cy ? overlapped : commit;
                overlapped.cy -= shift;
            } else {
                overlapped = oParent.cy > parent.cy ? overlapped : commit;
                overlapped.cy += shift;
            }

            preventOverlap(overlapped, view);
        }
    };

    applyBranchlessClass = function (selection) {
        if (selection.empty()) {
            return;
        }

        selection.classed('branchless', function (d) {
            return d.branchless;
        });

        if (selection.classed('commit-pointer')) {
            selection.attr('marker-end', function (d) {
                return d.branchless ? FADED_MARKER_END : REG_MARKER_END;
            });
        } else if (selection.classed('merge-pointer')) {
            selection.attr('marker-end', function (d) {
                return d.branchless ? FADED_MARKER_END : MERGE_MARKER_END;
            });
        }
    };

    cx = function (commit, view) {
        var parentcx = 0;
        for (var i=0; i<commit.parents.length; i++)
            parentcx = parentcx < commit.parents[i].cx ? commit.parents[i].cx : parentcx;
        return parentcx + (view.commitRadius * 4.5);
    };

    cy = function (commit, view) {
        var parent = commit.parents[0],
            parentCY = parent.cy || cy(parent, view),
            baseLine = view.baseLine,
            shift = view.commitRadius * 4.5,
            branches = [], // count the existing branches
            branchIndex = 0;

        for (var i = 0; i < view.commitData.length; i++) {
            var d = view.commitData[i];

            if (d.parents[0].id === commit.parents[0].id) {
                branches.push(d.id);
            }
        }

        branchIndex = branches.indexOf(commit.id);

        if (parentCY === baseLine) {
            var direction = 1;
            for (var bi = 0; bi < branchIndex; bi++) {
                direction *= -1;
            }

            shift *= Math.ceil(branchIndex / 2);

            return parentCY + (shift * direction);
        }

        if (parentCY < baseLine) {
            return parentCY - (shift * branchIndex);
        } else if (parentCY > baseLine) {
            return parentCY + (shift * branchIndex);
        }
    };

    fixCirclePosition = function (selection) {
        selection
            .attr('cx', function (d) {
                return d.cx;
            })
            .attr('cy', function (d) {
                return d.cy;
            });
    };

    // calculates the x1 point for commit pointer lines
    px1 = function (view, commit, parent) {
        var startCX = commit.cx,
            diffX = startCX - parent.cx,
            diffY = parent.cy - commit.cy,
            length = Math.sqrt((diffX * diffX) + (diffY * diffY));

        return startCX - (view.pointerMargin * (diffX / length));
    };

    // calculates the y1 point for commit pointer lines
    py1 = function (view, commit, parent) {
        var startCY = commit.cy,
            diffX = commit.cx - parent.cx,
            diffY = parent.cy - startCY,
            length = Math.sqrt((diffX * diffX) + (diffY * diffY));

        return startCY + (view.pointerMargin * (diffY / length));
    };

    fixPointerStartPosition = function (selection, view) {
        selection.attr('x1', function (d) {
            return px1(view, d[0], d[1]);
        }).attr('y1', function (d) {
            return py1(view, d[0], d[1]);
        });
    };

    px2 = function (view, commit, parent) {
        var endCX = parent.cx,
            diffX = commit.cx - endCX,
            diffY = parent.cy - commit.cy,
            length = Math.sqrt((diffX * diffX) + (diffY * diffY));

        return endCX + (view.pointerMargin * 1.2 * (diffX / length));
    };

    py2 = function (view, commit, parent) {
        var endCY = parent.cy,
            diffX = commit.cx - parent.cx,
            diffY = endCY - commit.cy,
            length = Math.sqrt((diffX * diffX) + (diffY * diffY));

        return endCY - (view.pointerMargin * 1.2 * (diffY / length));
    };

    fixPointerEndPosition = function (selection, view) {
        selection.attr('x2', function (d) {
            return px2(view, d[0], d[1]);
        }).attr('y2', function (d) {
            return py2(view, d[0], d[1]);
        });
    };

    fixIdPosition = function (selection, view, delta) {
        selection.attr('x', function (d) {
            return d.cx;
        }).attr('y', function (d) {
            return d.cy + view.commitRadius + delta;
        });
    };

    refY = function refY(t, view) {
        var commit = view.getCommit(t.commit),
            commitCY = commit.cy,
            refs = commit.refs,
            refIndex = refs.indexOf(t.name);

        if (refIndex === -1) {
            refIndex = refs.length;
        }

        if (commitCY < (view.baseLine)) {
            return commitCY - 45 - (refIndex * 25);
        } else {
            return commitCY + 50 + (refIndex * 25);
        }
    };

    /**
     * @class HistoryView
     * @constructor
     */
    function HistoryView(config) {
        var commitData = config.commitData || [],
            commit;

        /* We're generating random ID's, but don't want them to be completely random */
        this.name = config.name || 'UnnamedHistoryView';
        this.rng = seedrandom(this.name);

        for (var i = 0; i < commitData.length; i++) {
            commit = commitData[i];
            !commit.idx && (commit.idx = 'no-idx');
            !commit.id && (commit.id = this.generateId());
            !(commit.parents && (commit.parents.length >= 1)) && (commit.parents = ['initial']);
            !commit.refs && (commit.refs = []);
        }

        this.commitData = commitData;

        this.refs = [];
        this.HEAD = config.HEAD || 'ref: refs/heads/master';

        this.width = config.width;
        /*
        this.height = config.height || 400;
        this.orginalBaseLine = config.baseLine;
        this.baseLine = this.height * (config.baseLine || 0.6);
        */
        this.height = config.items[0] * 25 + 90 + (config.items[1]-1) * 90 + config.items[2] * 25;
        this.baseLine = this.height - config.items[0] * 25 - 50;

        this.commitRadius = config.commitRadius || 20;
        this.pointerMargin = this.commitRadius * 1.3;

        this.isRemote = !!config.remote;

        this.remoteName = config.remoteName;

        this.initialCommit = {
            id: 'initial',
            parents: [],
            cx: -(this.commitRadius * 2),
            cy: this.baseLine
        };
        for (var i = 0; i < commitData.length; i++) {
            commit = commitData[i];
            for (var j = 0; j < commit.parents.length; j++) {
                commit.parents[j] = this.getCommit(commit.parents[j]);
            }
        }

        this.remotes = {};

    }

    HistoryView.prototype = {
        generateId: function () {
            return Math.floor((1 + this.rng()) * 0x10000000).toString(16).substring(1);
        },

        /**
         * @method getCommit
         * @param ref {String} the id or a ref name that refers to the commit
         * @return {Object} the commit datum object
         */
        getRef: function(ref) {
            if (this.refs.indexOf('refs/tags/' + ref) != -1)
                return 'refs/tags/' + ref;
            if (this.refs.indexOf('refs/heads/' + ref) != -1)
                return 'refs/heads/' + ref;
            if (this.refs.indexOf('refs/remotes/' + ref) != -1)
                return 'refs/remotes/' + ref;
            return ref;
        },
        getCommit: function getCommit(ref) {
            var commitData = this.commitData, walker, matchedCommit = null, x, m;
            m = ref.match(/^(.*?)([~^][~^0-9]*)?$/);
            ref = m[1]; walker = m[2];

            if (ref === 'initial')
                return this.initialCommit;

            if (ref === 'HEAD') {
                if (this.HEAD.indexOf('ref: ') == 0)
                    ref = this.HEAD.slice(5);
                else
                    ref = this.HEAD
            }

            for (var i = 0; i < commitData.length; i++) {
                var commit = commitData[i];
                if (commit === ref) {
                    matchedCommit = commit;
                    break;
                }

                if (commit.id === ref) {
                    matchedCommit = commit;
                    break;
                }

                if (commit.idx === ref) {
                    matchedCommit = commit;
                    break;
                }

                var matchedRef = function() { 
                    for (var j = 0; j < commit.refs.length; j++) {
                        var refx = commit.refs[j];
                        if (refx === ref || (refx.search(new RegExp("^refs/(heads|tags|remotes)/" + ref + "$")) != -1)) {
                            matchedCommit = commit;
                            return true;
                        }
                    }
                }();
                if (matchedRef === true) {
                    break;
                }
            }

            var re = /[~^][0-9]*/g;
            while((m = re.exec(walker)) && matchedCommit) {
                m = m[0];
                if(m[0] == '^')
                    matchedCommit = matchedCommit.parents[parseInt(m.slice(1) || 1) - 1];
                else
                    for(var i=parseInt(m.slice(1) || 1); i && matchedCommit; i--)
                        matchedCommit = matchedCommit.parents[0];
            }

            return matchedCommit;
        },
        getCommits: function(ref) {
            if (ref.indexOf('..') == -1) {
                return [this.getCommit(ref)];
            }
            var ret = [];
            var m = ref.match(/^(.*)\.\.(.*)$/);
            var start = this.getCommit(m[1] || 'HEAD');
            var end = this.getCommit(m[2] || 'HEAD');
            /* Do a really stupid search, only good enough for these simple examples */
            for (var i=0; i<this.commitData.length; i++) {
                var commit = this.commitData[i];
                if((!this.isAncestor(commit, start)) && this.isAncestor(commit, end))
                    ret.push(commit);
            }
            return ret;
        },

        /**
         * @method render
         * @param container {String} selector for the container to render the SVG into
         */
        render: function (container) {
            var svgContainer, svg;

            svgContainer = container.append('div')
                .classed('svg-container', true)
                .classed('remote-container', this.isRemote);
                
            svg = svgContainer.append('svg:svg');

            svg.attr('id', this.name)
                .attr('width', this.width)
                .attr('height', this.height);

            if (this.isRemote) {
                svg.append('svg:text')
                    .classed('remote-name-display', true)
                    .text('Remote: ' + this.remoteName)
                    .attr('x', 5)
                    .attr('y', 15);
            }

            this.svgContainer = svgContainer;
            this.svg = svg;
            this.arrowBox = svg.append('svg:g').classed('pointers', true);
            this.commitBox = svg.append('svg:g').classed('commits', true);
            this.refBox = svg.append('svg:g').classed('refs', true);
            this.renderCommits();
        },

        destroy: function () {
            this.svg.remove();
            this.svgContainer.remove();
            clearInterval(this.refreshSizeTimer);

            for (var prop in this) {
                if (this.hasOwnProperty(prop)) {
                    this[prop] = null;
                }
            }
        },

        _calculatePositionData: function () {
            for (var i = 0; i < this.commitData.length; i++) {
                var commit = this.commitData[i];
                commit.cx = cx(commit, this);
                commit.cy = cy(commit, this);
                preventOverlap(commit, this);
            }
        },
        
        _resizeSvg: function() {
            var ele = document.getElementById(this.svg.node().id);
            var container = ele.parentNode;
            var currentWidth = ele.offsetWidth;
            var newWidth;

            if (ele.getBBox().width > container.offsetWidth)
                newWidth = Math.round(ele.getBBox().width);
            else
                newWidth = container.offsetWidth - 5;

            if (currentWidth != newWidth) {
                this.svg.attr('width', newWidth);
                container.scrollLeft = container.scrollWidth;
            }
        },

        renderCommits: function () {
            if (typeof this.height === 'string' && this.height.indexOf('%') >= 0) {
                var perc = this.height.substring(0, this.height.length - 1) / 100.0;
                var baseLineCalcHeight = Math.round(this.svg.node().parentNode.offsetHeight * perc) - 65;
                var newBaseLine = Math.round(baseLineCalcHeight * (this.originalBaseLine || 0.6));
                if (newBaseLine !== this.baseLine) {
                    this.baseLine = newBaseLine;
                    this.initialCommit.cy = newBaseLine;
                    this.svg.attr('height', baseLineCalcHeight);
                }
            }
            this._calculatePositionData();
            this._calculatePositionData(); // do this twice to make sure
            this._renderCircles();
            this._renderPointers();
            this._renderIdLabels();
            this._resizeSvg();
            this.renderRefs();
        },

        _renderCircles: function () {
            var view = this,
                existingCircles,
                newCircles,
                head = this.getCommit('HEAD');

            existingCircles = this.commitBox.selectAll('circle.commit')
                .data(this.commitData, function (d) { return d.id; })
                .attr('id', function (d) {
                    return view.name + '-' + d.id;
                })
                .classed('checked-out', function (d) {
                    return d.id === head.id;
                });

            existingCircles.transition()
                .duration(500)
                .call(fixCirclePosition);

            newCircles = existingCircles.enter()
                .append('svg:circle')
                .attr('id', function (d) {
                    return view.name + '-' + d.id;
                })
                .classed('commit', true)
                .classed('merge-commit', function (d) {
                    return d.parents.length > 1;
                })
                .classed('checked-out', function (d) {
                    return d.id === head.id;
                })
                .call(fixCirclePosition)
                .attr('r', 1)
                .transition()
                .duration(500)
                .attr('r', this.commitRadius);

        },

        _renderPointers: function () {
            var view = this,
                existingPointers,
                newPointers;

            var data = [];
            for(var i = 0; i < this.commitData.length; i++)
                for(var j = 0; j < this.commitData[i].parents.length; j++)
                    data.push([this.commitData[i], this.commitData[i].parents[j], j>0])

            existingPointers = this.arrowBox.selectAll('line.pointer')
                .data(data, function (d) {
                    return view.name + '-' + d[0].id + '-to-' + d[1].id;
                })
                .attr('id', function (d) {
                    return view.name + '-' + d[0].id + '-to-' + d[1].id;
                });

            existingPointers.transition()
                .duration(500)
                .call(fixPointerStartPosition, view)
                .call(fixPointerEndPosition, view);

            newPointers = existingPointers.enter()
                .append('svg:line')
                .attr('id', function (d) {
                    return view.name + '-' + d[0].id + '-to-' + d[1].id;
                })
                .classed('pointer', true)
                .classed('commit-pointer', function(d) { return !d[2]; })
                .classed('merge-pointer', function(d) { return d[2]; })
                .call(fixPointerStartPosition, view)
                .attr('x2', function () { return d3.select(this).attr('x1'); })
                .attr('y2', function () {  return d3.select(this).attr('y1'); })
                .attr('marker-end', function(d) { return d[2] ? MERGE_MARKER_END : REG_MARKER_END; })
                .transition()
                .duration(500)
                .call(fixPointerEndPosition, view);
        },

        _renderIdLabels: function () {
            this._renderText('id-label', function (d) { return d.id + '..'; }, 14);
            this._renderText('message-label', function (d) { return d.message; }, 24);
        },

        _renderText: function(className, getText, delta) {
            var view = this,
                existingTexts,
                newtexts;

            existingTexts = this.commitBox.selectAll('text.' + className)
                .data(this.commitData, function (d) { return d.id; })
                .text(getText);

            existingTexts.transition().call(fixIdPosition, view, delta);

            newtexts = existingTexts.enter()
                .insert('svg:text', ':first-child')
                .classed(className, true)
                .text(getText)
                .call(fixIdPosition, view, delta);
        },

        _parseRefData: function () {
            var refData = [{name: 'HEAD', commit: this.getCommit('HEAD').id}], i;

            for (i = 0; i < this.commitData.length; i++) {
                var c = this.commitData[i];

                for (var t = 0; t < c.refs.length; t++) {
                    var refName = c.refs[t];
                    if (this.refs.indexOf(refName) === -1) {
                        this.refs.push(refName);
                    }

                    refData.push({name: refName, commit: c.id});
                }
            }
            return refData;
        },

        _markBranchlessCommits: function () {
            var branch, commit, commits = [], c, b;

            // first mark every commit as branchless
            for (c = 0; c < this.commitData.length; c++) {
                this.commitData[c].branchless = true;
            }

            for (b = 0; b < this.refs.length; b++) {
                branch = this.refs[b];
                commits.push(this.getCommit(branch));
            }
            while(commits.length) {
                commit = commits.shift();
                if(!commit.branchless)
                    continue;
                commit.branchless = false;
                for (c = 0; c < commit.parents.length; c++) {
                    commits.push(commit.parents[c]);
                }
            }

            this.svg.selectAll('circle.commit').call(applyBranchlessClass);
            this.svg.selectAll('line.commit-pointer').call(applyBranchlessClass);
            this.svg.selectAll('polyline.merge-pointer').call(applyBranchlessClass);
        },

        renderRefs: function () {
            var view = this,
                refData = this._parseRefData(),
                existingRefs, newRefs;

            existingRefs = this.refBox.selectAll('g.ref')
                .data(refData, function (d) { return d.name; });

            existingRefs.exit().remove();

            existingRefs.select('rect')
                .transition()
                .duration(500)
                .attr('y', function (d) { return refY(d, view); })
                .attr('x', function (d) {
                    var commit = view.getCommit(d.commit),
                        width = Number(d3.select(this).attr('width'));

                    return commit.cx - (width / 2);
                });

            existingRefs.select('text')
                .transition()
                .duration(500)
                .attr('y', function (d) { return refY(d, view) + 14; })
                .attr('x', function (d) {
                    var commit = view.getCommit(d.commit);
                    return commit.cx;
                });

            newRefs = existingRefs.enter()
                .append('g')
                .attr('class', function (d) {
                    var classes = 'ref';
                    if (d.name.indexOf('refs/tags/') === 0) {
                        classes += ' tag';
                    } else if (d.name.indexOf('refs/remotes/') >= 0) {
                        classes += ' remote';
                    } else if (d.name === 'HEAD') {
                        classes += ' head';
                    }
                    return classes;
                });

            newRefs.append('svg:rect')
                .attr('width', function (d) {
                    return (d.name.match(/(refs\/(heads|tags|remotes)\/)?(.*)/)[3].length * 6 + 10);
                })
                .attr('height', 20)
                .attr('y', function (d) { return refY(d, view); })
                .attr('x', function (d) {
                    var commit = view.getCommit(d.commit),
                        width = Number(d3.select(this).attr('width'));

                    return commit.cx - (width / 2);
                });

            newRefs.append('svg:text')
                .text(function (d) {
                    return d.name.match(/(refs\/(heads|tags|remotes)\/)?(.*)/)[3];
                })
                .attr('y', function (d) {
                    return refY(d, view) + 14;
                })
                .attr('x', function (d) {
                    var commit = view.getCommit(d.commit);
                    return commit.cx;
                });

            this._markBranchlessCommits();
        },

        moveRef: function (ref, commit) {
            var currentLoc = this.getCommit(ref);

            if (currentLoc) {
                currentLoc.refs.splice(currentLoc.refs.indexOf(ref), 1);
            }

            commit.refs.push(ref);
            return this;
        },

        moveHead: function (commit) {
            if(this.HEAD.indexOf('ref: ') == 0)
                this.moveRef(this.HEAD.slice(5), commit);
            else
                this.HEAD = commit.id;
        },

        /**
         * @method isAncestor
         * @param ref1
         * @param ref2
         * @return {Boolean} whether or not ref1 is an ancestor of ref2
         */
        isAncestor: function isAncestor(ref1, ref2) {
            var currentCommit = ref1, targetTree = ref2, c;

            if (targetTree.id === currentCommit.id)
                return true;

            for (c = 0; c < targetTree.parents.length; c++) {
                if (this.isAncestor(currentCommit, targetTree.parents[c]))
                    return true;
            }
            return false;
        },

        _commit: function (commit) {
            var head = this.getCommit('HEAD');
            commit = commit || {};

            !commit.id && (commit.id = this.generateId());
            !commit.refs && (commit.refs = []);
            !commit.parents && (commit.parents = []);
            !commit.message && (commit.message = null);

            if (!commit.parents.length)
                commit.parents = [head];

            this.commitData.push(commit);
            /* Only move HEAD or refs if we're adding on top of HEAD */
            for (var i=0; i<commit.parents.length; i++) {
                if (commit.parents[i].id == head.id) {
                    this.moveHead(commit);
                    break
                }
            }
            return commit;
        },

        commit: function (commit, amend) {
            if (amend) {
                commit = commit || this.getCommit('HEAD');
                var ncommit = { parents: commit.parents, message: commit.message }
                this.reset('HEAD^');
                commit = this._commit(ncommit);
            }
            else {
                commit = this._commit(commit);
            }
            this.renderCommits();
            return commit;
        },

        ref: function (name, commit, force) {
            commit = this.getCommit(commit || 'HEAD');

            if (!name || name.trim() === '') {
                throw new Error('You need to give a refname.');
            }

            if (name === 'HEAD') {
                throw new Error('You cannot name your ref "HEAD".');
            }

            if (name.indexOf(' ') > -1) {
                throw new Error('refnames cannot contain spaces.');
            }

            if (this.refs.indexOf(name) > -1) {
                if (force)
                    this.moveRef(name, commit);
                else
                    throw new Error('Ref "' + name + '" already exists.');
            }
            else {
                commit.refs.push(name);
                this.refs.push(name);
            }
            this.renderRefs();
            return this;
        },
        
        branch: function(name, commit, force) {
            this.ref('refs/heads/' + name, commit, force);
        },

        tag: function (name, commit, force) {
            this.ref('refs/tags/' + name, commit, force);
        },

        deleteRef: function (name) {
            var refIndex,
                commit;

            if (!name || name.trim() === '') {
                throw new Error('You need to give a branch name.');
            }

            if (name === this.HEAD.slice(5)) {
                throw new Error('Cannot delete the currently checked-out branch.');
            }

            refIndex = this.refs.indexOf(name);

            if (refIndex === -1) {
                throw new Error('That branch doesn\'t exist.');
            }

            this.refs.splice(refIndex, 1);
            commit = this.getCommit(name);
            refIndex = commit.refs.indexOf(name);

            if (refIndex > -1) {
                commit.refs.splice(refIndex, 1);
            }

            this.renderRefs();
        },

        checkout: function (ref) {
            ref = this.getRef(ref);
            var commit = this.getCommit(ref);

            if (!commit) {
                throw new Error('Cannot find commit: ' + ref);
            }

            if (ref.indexOf('refs/heads/') === 0)
                this.HEAD = 'ref: ' + ref
            else
                this.HEAD = commit.id;

            this.renderRefs();
        },

        reset: function (ref) {
            var commit = this.getCommit(ref);

            if (!commit) {
                throw new Error('Cannot find ref: ' + ref);
            }

            this.moveHead(commit);
            this.renderRefs();
        },

        revert: function (ref) {
            var commits = this.getCommits(ref), head = this.getCommit('HEAD');

            if (!commits.length) {
                throw new Error('No commits to revert')
            }

            for (var i=0; i<commits.length; i++) {
                var commit = commits[i];
                if (!this.isAncestor(commit, head)) {
                    throw new Error(commit.id + ' is not an ancestor of HEAD.');
                }
                this._commit({'message': 'Revert: ' + commit.id});
            }
            this.renderCommits();
        },

        fastForward: function (ref) {
            var commit = this.getCommit(ref);
            if (!commit) {
                throw new Error('Cannot find ref: ' + ref);
            }
            if (!isAncestor(this.getCommit('HEAD'), commit)) {
                throw new Error('Cannot find fast-forward to a commit that is not a direct descendant');
            }
            this.moveHead(commit);
            this.renderRefs();
        },

        merge: function (ref, noFF) {
            var mergeTarget = this.getCommit(ref),
                currentCommit = this.getCommit('HEAD');

            if (!mergeTarget) {
                throw new Error('Cannot find ref: ' + ref);
            }

            if (currentCommit.id === mergeTarget.id) {
                throw new Error('Already up-to-date.');
            } else if (this.isAncestor(mergeTarget, currentCommit)) {
                throw new Error('Already up-to-date.');
            } else if (this.isAncestor(currentCommit, mergeTarget) && !noFF) {
                this.fastForward(mergeTarget);
                return 'Fast-Forward';
            } else {
                this.commit({parents: [currentCommit, mergeTarget]});
            }
        },

        rebase: function (onto, start, end) {
            var rebaseTarget = this.getCommit(onto),
                currentCommit = this.getCommit(end || 'HEAD'),
                startCommit = start ? this.getCommit(start) : undefined,
                rebaseTreeLoc,
                rebaseMessage,
                toRebase = [],
                rebasedCommit;

            if (!rebaseTarget) {
                throw new Error('Cannot find ref: ' + ref);
            }

            if (this.isAncestor(rebaseTarget, currentCommit)) {
                throw new Error('Already up-to-date.');
            }

            while (!this.isAncestor(currentCommit, startCommit || rebaseTarget)) {
                toRebase.unshift(currentCommit);
                currentCommit = currentCommit.parents[0];
            }

            for (var i = 0; i < toRebase.length; i++) {
                var parents = toRebase[i].parents.slice(1);
                parents.unshift(rebaseTarget);
                rebaseTarget = this._commit({parents: parents, 'message': 'From: ' + toRebase[i].id});
            }

            if (end) {
                var ref = this.getRef(end);
                if (this.refs.indexOf(ref) != -1) {
                    this.moveRef(ref, rebaseTarget);
                    this.checkout(ref);
                }
            }
            else {
                this.moveHead(rebaseTarget);
            }
            this.renderCommits();
        },

        fetch: function(remote) {
            remote = remote || 'origin';
            remote = this.remotes[remote];
            for(var i=0; i<remote.refs.length; i++) {
                var ref = remote.refs[i];
                if (ref.indexOf('refs/tags/') != 0 && ref.indexOf('refs/heads/') != 0)
                    continue;
                var commit = this._fetchCommit(remote, ref);
                ref = ref.replace('refs/heads/', 'refs/remotes/' + remote.remoteName + '/');
                if (this.refs.indexOf(ref) < 0)
                    this.refs.push(ref);
                this.moveRef(ref, commit);
            }
            this.renderCommits();
        },

        _fetchCommit: function(remote, commit) {
            var rc = remote.getCommit(commit);
            for (var i=0; i<rc.parents.length; i++) {
                this._fetchCommit(remote, rc.parents[i].id)
            }
            var lc = this.getCommit(rc.id);
            if (!lc) {
                lc = {
                    id: rc.id,
                    idx: rc.idx,
                    message: rc.message,
                    parents: [],
                    refs: []
                }
                for(var i=0; i<rc.parents.length; i++)
                    lc.parents.push(this.getCommit(rc.parents[i].id));
                this.commitData.push(lc);
            }
            return lc;
        },

        cherry_pick: function(ref) {
            var commits = this.getCommits(ref);
            var head  = this.getCommit('HEAD');
            if (!commits.length) {
                throw new Error('No commits to cherry pick')
            }
            for (var i=0; i<commits.length; i++) {
                var commit = commits[i];
                if (this.isAncestor(commit, head)) {
                    throw new Error('Cannot cherry-pick my own (grand)parents');
                }
                this._commit({'message':  'From: ' + commit.id});
            }

            this.renderCommits();
        }

    };

    return HistoryView;
});
