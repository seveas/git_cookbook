Title: Serving git's smart http protocol and a web interface with Apache
Date: 2015-11-03
Category: Git on the server
Slug: apache-gitweb-cgit-smart-http
Tags: apache, server, gitweb, cgit, http, https, authentication
Summary: From zero to fully working web server in 2 configuration files, including smart HTTP, ssl, authentication, and cgit or gitweb.

While git's original http protocol implementation was fairly dumb, the
so-called smart http protocol is much better to use and rivals the ssh and git
protocols in speed.

It can also be combined with a web-based repository viewer such as gitweb or
cgit, allowing you to serve repositories and their web-based viewers from the
same location. With bonus points for also supporting the dumb http protocol for
really old git clients.

The git documentation has some example configurations, but not really one that
ties all this together. The configuration below lets you create a git http server
that lets users browse your repositories with cgit or gitweb, and clone using
both dumb and smart http.

    :::apache
    File: apache-http.conf

The following minimal configuration is needed for gitweb:

    :::perl
    File: gitweb.conf

And for cgit, this is the absolute minimum

    :::cfg
    File: cgitrc

HTTPS and authentication
------------------------
The configuration above is absolutely fine if you want world-readable read-only
repositories and all your pushing is done over ssh. But if you want to accept
pushes over https, you mush also enable authentication and SSL. The
configuration below does this. It does not do anything with authorization
though, it assumes that if you can authenticate, you can push. If you want more
fine-grained authorization you can use per-repository [update
hooks](/manpages/githooks.html) to authorize your users.

As you'll see, the configuration is very similar to the http-only version. The
differences are that all http traffic is redirected to https, ssl is enabled
and secured using Mozilla's recommendations and that authentication is
mandatory.

    :::apache
    File: apache-https.conf
