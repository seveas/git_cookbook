Title: Using credential helpers to cache passwords
Category: Effective git usage
Date: 2015-11-04
Tags: authentication, http
Summary: Never type your passwords again, but store them safely in a password manager Git can use.

Various git commands need to know your credentials to access services:

* HTTP transfers to authenticated sources (git fetch/push)
* Proxy authentication for those transfers
* The MediaWiki remote
* The git-send-email command

All these commands, and also various third-party add-ons such as git-spindle,
use a common subsystem to retrieve these credentials. By default these just ask
you for a username and password, but you can configure credential helpers to
avoid typing these all the time.

The simplest is the 'cache' helper, which caches any credential in memory for a
number of seconds (by default 15 minutes). Enabling it is simple:

    :::console
    $ git config --global credential.helper cache

Or if you want to use a nonstandard timeout:

    :::console
    $ git config --global credential.helper 'cache --timeout=3600'

It works transparently as you can see in the following output

    :::console
    $ git clone https://git.example.com/example.git
    Cloning into 'example'...
    Username for 'https://git.example.com': dennis
    Password for 'https://dennis@git.example.com':
    remote: Counting objects: 404, done.
    remote: Compressing objects: 100% (261/261), done.
    remote: Total 404 (delta 226), reused 235 (delta 131)
    Receiving objects: 100% (404/404), 124.59 KiB | 0 bytes/s, done.
    Resolving deltas: 100% (226/226), done.
    Checking connectivity... done.
    $ git -C example ls-remote
    From https://git.example.com/example.git
    7c3c37ba945276ca872217850ab8ceeb2e7249e5    HEAD
    7c3c37ba945276ca872217850ab8ceeb2e7249e5    refs/heads/master

While the cache helper is certainly useful, it still makes you type in your
password regularly. There are a few other helpers that actually store your
password on disk, some more secure than others.

    :::console
    git config --global credential.helper store
    git config --global credential.helper netrc

The store and netrc helpers use unencrypted plain-text files for storing
credentials. The store helper has its own format, the netrc helper will read
your .netrc file. These are useful if you cannot use any of the credential
helpers below, which all require a desktop environment.

    :::console
    git config --global credential.helper store wincred
    git config --global credential.helper store osxkeychain
    git config --global credential.helper store gnome-keyring

The wincred, osxkeychain and gnome-keyring credential helpers integrate with
the secure credential storage provided by Windows, OSX and Gnome. These are the
preferred credential helpers as they never store passwords unencrypted.

For KDE there is no helper to integrate with kwallet. Instead, KDE sets the
SSH_ASKPASS variable to ksshaskpass, which then integrates with kwallet. It's a
fairly poor integration with git as you will need to modify your urls for your
remotes to include your username, e.g. from https://git.example.com to https://dennis@git.example.com.
