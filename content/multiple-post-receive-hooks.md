Title: Using multiple post-receive hooks
Category: Git on the server
Date: 2015-11-03
Tags: hooks, post-receive
Summary: Workaround for a common pitfall for some git hooks: consuming all input.

The most commonly used post-receive hook is probably one that sends mail
about incoming pushes. Git ships one that works reasonably well, and there's
also the git-multimail hook.

But what if you want to do more than one thing in the post-receive hook? You
cannot easily run more than one hook, as the hook needs to consume all input on
stdin. So the most common approach is to hack something in the hook, making
upgrading git more painful as this needs to be forward-ported all the time.

So instead, please use `pee` from the
[moreutils](https://joeyh.name/code/moreutils/) project to multiplex the input
the hook gets and you can use as many hooks as you want

The default hook post-receive hook:

    #!/bin/sh
    . /usr/share/doc/git-core/contrib/hooks/post-receive-email

A hook that runs several more hooks (in this case: submitting the job to a
Jenkins CI server and scheduling a push to a mirror repository):

    #!/bin/sh
    /usr/bin/pee /usr/share/doc/git/contrib/hooks/post-receive-email \
                 /srv/gitroot/custom-hooks/jenkins \
                 /srv/gitroot/custom-hooks/schedule-sync
