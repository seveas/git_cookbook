Title: About the Git cookbook
Slug: about

The Git cookbook is a collection of recipes for using git and pages with
background information. It is written and maintained by Dennis Kaarsemaker,
based on many years of using, supporting  and troubleshooting git in the
workplace and on IRC, maintaining git servers for tens to hundreds of
developers and building tools with and around git. The cookbook aims to provide
solutions for common pitfalls, insight into git's interface and inner workings
and tips & tricks for recovering from problems caused by git or using git
wrong.

If you have a question, comment or suggestion about a specific article, please
leave a comment on the article or in the repo on
[GitLab](https://gitlab.com/seveas/git_cookbook). Suggestions for subjects of
new articles are also welcome, the cookbook needs to grow!

If you are looking for support for git problems, come visit us in the #git,
\#github or #gitlab channels on the freenode network. You'll git by with a
little help from your friends ([Sorry
Paul...](https://www.youtube.com/watch?v=SkyqRP8S93Y))

### Thanks to
* Yves Orton for forcing me to use git all those years ago
* The Git developers (bet you didn't see that coming...)
* The [Pelican project](http://blog.getpelican.com/), the software used to
  maintain the cookbook.
* Alexandre Vicenzi for the [Flex](https://github.com/alexandrevicenzi/Flex/)
  theme.
* Wei Wang for [explain git with
  d3](https://github.com/onlywei/explain-git-with-d3), of which we use a
  heavily modified copy for the commit graphs.
* The people in #git and #github for ideas, solutions and fun times.

### License
Except the content excluded below, all content on this site is ©Dennis
Kaarsemaker, all rights reserved. Content from this site may not be reproduced
or reused without explicit permission from the author.

All manpages under [/manpages](/manpages) have been copied from the Git project
and are licensed under the GNU GPL, version 2 of that license.

All code and configuration samples are [dedicated to the public
domain](https://creativecommons.org/publicdomain/zero/1.0/), which means you
may use, copy, modify and distribute them as you wish with no obligations.
