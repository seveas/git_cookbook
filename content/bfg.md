Title: Removing unwanted data from git repositories
Category: Commit grooming
Date: 2015-11-05
Tags: filter-branch, bfg, cleanup, rewrite
Summary: Ever committed something very large or sensitive? Find out how you can remove it from your repository.

A very common mistake is to commit sensitive data, build products or otherwise
data that should never end up in a git repository. Git has an extremely
powerful command to deal with this: `git filter-branch`. This command is not
only extremely powerful, it's also nigh incomprehensible.

We'll cover filter-branch and its capabilities in later articles, but for today
we'll stick with a much simpler tool that's been created just for cleaning up
unwanted files and sensitive data (think passwords).

The BFG repo cleaner
--------------------
The BFG repo cleaner cannot do everything git-filter branch can, but the things
it does, it does extremely well, fast and easy to use. So easy that I'm going
to be lazy and just copy some examples from the BFG website:

Removing SSH private keys:

    :::console
    $ java -jar bfg.jar --delete-files id_{rsa,dsa} example.git

Removing huge files:

    :::console
    $ java -jar bfg.jar --strip-blobs-bigger-than 50M example.git

Removing passwords:

    :::console
    $ java -jar bfg.jar --replace-text passwords.txt example.git

It can also replace text based on regular expressions, delete entire folders
and convert huge blobs to GitHub's Git LFS.

[Download the BFG repo cleaner here](https://rtyley.github.io/bfg-repo-cleaner/)
