Title: Dealing with misconfigured https repositories
Category: Troubleshooting
Date: 2015-11-04
Tags: http, https, danger
Summary: You should never have to disable SSL verification, but sometimes you
    do not control the host you must get some data from. If you must do so,
    learn here how to do this without firther compromising your security.

!!! warning "Disabling SSL certificate verification puts your data at risk"
    This article was written to counteract some really stupid advice to disable
    SSL certificate verification completely. Never *ever* do `git config
    --global http.sslVerify false` or a grue will absolutely eat you.

When dealing with an https host that has a misconfigured SSL certificate, such
as a self-signed or expired certificate, the correct action to take is to fix
the problem with that certificate. But you're not always in the position to do
so, so there are workarounds.

Git will let you disable SSL certificate verification on a global, per host or
per command invocation basis. But before you do any of this, you have to
understand that this is a really bad thing: it opens you up to
man-in-the-middle attacks and you should really consider all data (including
passwords) sent this way to be compromised.

With that lecture out of the way, here's how you actually do this without
compromising security too much.

    :::console
    $ git clone https://git.example.com/example.git
    Cloning into 'example'...
    fatal: unable to access 'https://git.example.com/example.git/': SSL: certificate subject name (www.example.com) does not match target host name 'git.example.com'
    $ git -c http.sslVerify=False clone https://git.example.com/example.git
    remote: Counting objects: 404, done.
    remote: Compressing objects: 100% (261/261), done.
    remote: Total 404 (delta 227), reused 235 (delta 131)
    Receiving objects: 100% (404/404), 124.40 KiB | 0 bytes/s, done.
    Resolving deltas: 100% (227/227), done.
    Checking connectivity... done.

Of course typing `-c http.sslVerify=false` for every clone, fetch and push from
that host can get cumbersome, so let's create some aliases.

    :::console
    $ git config alias.unsafe-clone 'clone -c http.sslVerify=false'
    $ git config alias.unsafe-fetch 'fetch -c http.sslVerify=false'
    $ git config alias.unsafe-push 'push -c http.sslVerify=false'
    $ git config alias.unsafe-pull 'pull -c http.sslVerify=false'
    $ git config alias.unsafe-ls-remote 'ls-remote -c http.sslVerify=false'

And now we can clone from such misconfigured hosts:

    :::console
    $ git unsafe-clone https://git.example.com/example.git
    remote: Counting objects: 404, done.
    remote: Compressing objects: 100% (261/261), done.
    remote: Total 404 (delta 227), reused 235 (delta 131)
    Receiving objects: 100% (404/404), 124.40 KiB | 0 bytes/s, done.
    Resolving deltas: 100% (227/227), done.
    Checking connectivity... done.

If you think these unsafe-clone aliases are too opinionated and simply don't
want to care about your data's security, you can also tell git not to ever do
SSL certificate verification for this host in your .gitconfig

    :::ini
    [http "https://git.example.com/"]
    sslVerify=false

A slightly safer option than disabling SSL certificate verification is
configuring git to use a different CA certificate file for these broken hosts.
This is only safe if you can get the CA certificate in a secure way.

    :::ini
    [http "https://git.example.com/"]
    sslCAPath=/home/dennis/.config/ssl/example.com-ca.crt

And yes, you can even disable SSL certificate verification completely. But as I
said above, a grue will eat you if you do so.
